﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionType {
	Speech,
	Wait,
	GoTo,
    Idle
};

public struct Action{
	public ActionType type;
	public string message;
	public float time;
	public Vector3 target;
    public float speed;
    public bool completed;
};

public class CapsuleBehav : MonoBehaviour {

    private Queue<Action> actions = new Queue<Action>();
    private Action a;

	void Start ()
    {
        actions = ScriptLoader.Get().GetActionSequence(@"Assets\Lua\behav.lua");
        if(actions.Count > 0)
            a = actions.Dequeue();
	}

    void Update()
    {
        if (!a.completed)
        {
            switch (a.type)
            {
                case ActionType.GoTo:
                    transform.position = Vector3.MoveTowards(transform.position, a.target, a.speed * Time.deltaTime);
                    if (Vector3.Distance(transform.position, a.target) < 0.1)
                    {
                        a.completed = true;
                    }
                    break;
                case ActionType.Speech:
                    Debug.Log(a.message);
                    a.completed = true;
                    break;
                case ActionType.Wait:
                    if (a.time > 0)
                        a.time -= Time.deltaTime;
                    else
                        a.completed = true;
                    break;
            }
        }
        else if (a.completed && actions.Count > 0)
        {
            a = actions.Dequeue();
        }
        else
        {
            a = new Action();
            a.type = ActionType.Idle;
        }
    }
}