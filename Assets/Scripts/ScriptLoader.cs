﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using MoonSharp.Interpreter;
using UnityEngine;

public class ScriptLoader {

	private static ScriptLoader instance;
    private Queue<Action> result = new Queue<Action>();
    private DynValue obj;

    public ScriptLoader () {
        UserData.RegisterType<ScriptLoader>();
        UserData.RegisterType<Action>();
        UserData.RegisterType<Vector3>();
        obj = UserData.Create(this);
    }

    public static ScriptLoader Get () {
		if (instance == null)
			instance = new ScriptLoader ();
		return instance;
	}

	public Queue<Action> GetActionSequence (string luaFile) {
        result.Clear();

		StreamReader reader = new StreamReader (luaFile);
		string scriptCode = @reader.ReadToEnd ();
        Script script = new Script();
        script.Globals.Set("this", obj);
        script.DoString(scriptCode);

        return result;
	}

    public void Goto (float x, float y, float z, float speed) {
        Action a = new Action();
        a.type = ActionType.GoTo;
        a.target = new Vector3(x, y, z);
        a.speed = speed;
        result.Enqueue(a);
    }

    public void Wait (float time) {
        Action a = new Action();
        a.type = ActionType.Wait;
        a.time = time;
        result.Enqueue(a);
    }

    public void Speech (string text) {
        Action a = new Action();
        a.type = ActionType.Speech;
        a.message = text;
        result.Enqueue(a);
    }
}